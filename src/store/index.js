import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

import cameraStore from "./camera";
import gridStore from "./grid";

let storeCfg = {
  state: {},
  getters: {},
  mutations: {},
  actions: {}
};

storeCfg = cameraStore(storeCfg);
storeCfg = gridStore(storeCfg);

export default new Vuex.Store(storeCfg);
