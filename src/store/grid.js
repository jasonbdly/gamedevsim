import merge from "merge-anything";

export default function(storeCfg) {
    return merge(storeCfg, {
        state: {
            grid: {
                width: 25,
                height: 25,
                tileSize: 32,
                tiles: [[]]
            }
        },
        getters: {
            tileSize: state => {
                return state.grid.tileSize;
            },
            gridWidth: state => {
                return state.grid.width;
            },
            gridHeight: state => {
                return state.grid.height;
            },
            tileContents: state => (row, column) => {
                return (
                    (state.grid.tiles[row] && state.grid.tiles[row][column]) ||
                    "TestTile"
                );
            }
        },
        mutations: {
            initializeGrid(state) {
                state.grid.tiles = [];
                for (let row = 0; row < state.height; row++) {
                    state.grid.tiles.push([]);
                    for (let column = 0; column < state.width; column++) {
                        state.grid.tiles[row].push("TestTile");
                    }
                }
            },
            setTileContents(state, row, column, contents) {
                state.grid.tiles[row][column] = contents;
            }
        },
        actions: {
            initializeGrid({ commit }) {
                commit("initializeGrid");
            }
        }
    });
}
