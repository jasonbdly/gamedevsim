import merge from "merge-anything";

export default function(storeCfg) {
    return merge(storeCfg, {
        state: {
            camera: {
                x: 0,
                y: 0,
                rotX: 60,
                rotY: 0,
                rotZ: -45,
                moveSpeed: 25,
                zoom: 1
            }
        },
        getters: {
            cameraX: state => {
                return state.camera.x + "px";
            },
            cameraY: state => {
                return state.camera.y + "px";
            },
            cameraRotationX: state => {
                return state.camera.rotX + "deg";
            },
            cameraRotationY: state => {
                return state.camera.rotY + "deg";
            },
            cameraRotationZ: state => {
                return state.camera.rotZ + "deg";
            },
            cameraZoom: state => {
                return state.camera.zoom.toFixed(1);
            }
        },
        mutations: {
            setCameraX(state, amount) {
                state.camera.x = amount;
            },
            setCameraY(state, amount) {
                state.camera.y = amount;
            },
            moveCameraX(state, amount) {
                state.camera.x += amount;
            },
            moveCameraY(state, amount) {
                state.camera.y += amount;
            },
            zoomCamera(state, amount) {
                state.camera.zoom += amount;
            }
        },
        actions: {
            cameraPanUp({ commit, state }, amount) {
                amount = amount || state.camera.moveSpeed;

                commit("moveCameraY", amount);
                commit("moveCameraX", -amount);
            },
            cameraPanDown({ commit, state }, amount) {
                amount = amount || state.camera.moveSpeed;

                commit("moveCameraY", -amount);
                commit("moveCameraX", amount);
            },
            cameraPanLeft({ commit, state }, amount) {
                amount = amount || state.camera.moveSpeed;

                commit("moveCameraY", amount);
                commit("moveCameraX", amount);
            },
            cameraPanRight({ commit, state }, amount) {
                amount = amount || state.camera.moveSpeed;

                commit("moveCameraY", -amount);
                commit("moveCameraX", -amount);
            },
            cameraCenter({ commit }) {
                commit("setCameraX", -window.innerWidth / 10);
                commit("setCameraY", window.innerHeight / 3);
            },
            cameraZoomIncrease({ commit, state }, amount) {
                amount = amount || 0.1;

                if (state.camera.zoom <= 3 - amount) {
                    commit("zoomCamera", amount);
                }
            },
            cameraZoomDecrease({ commit, state }, amount) {
                amount = amount || 0.1;

                if (state.camera.zoom >= 0.5 + amount) {
                    commit("zoomCamera", -amount);
                }
            }
        }
    });
}
