import { TileFactory, Tile, registerTileFactory } from "./tile";

import imageURL from "../../assets/tiles/TestTile.png";

export class TestTileFactory extends TileFactory {
    constructor() {
        super("TestTile");
    }

    newTile() {
        return new Tile(this.tileName, imageURL);
    }
}

registerTileFactory(TestTileFactory);
