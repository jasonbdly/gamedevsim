let tileRegistry = {};
export function registerTileFactory(tileFactoryClass) {
    let tileFactory = new tileFactoryClass();
    if (tileRegistry[tileFactory.tileName]) {
        throw `Tile "${tileFactory.tileName}" already registered.`;
    }

    tileRegistry[tileFactory.tileName] = tileFactory;
}

export function createTile(tileName) {
    if (!tileRegistry[tileName]) {
        throw `Unknown tile "${tileName}".`;
    }

    return tileRegistry[tileName].newTile();
}

export class TileFactory {
    tileName = "";

    constructor(tileName) {
        this.tileName = tileName;
    }

    newTile() {
        throw "TileFactory.newTile must be overridden by the extension type.";
    }
}

// TODO: Add additional state info for tiles (animations, orientation, ???)
export class Tile {
    constructor(tileName, imageURL) {
        this.tileName = tileName;
        this.imageURL = imageURL;
    }
}