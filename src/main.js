import Vue from "vue";
import App from "./App.vue";
import store from "./store/";
import { mapActions } from "vuex";

import * as VueGL from "vue-gl";
Object.keys(VueGL).forEach(name => {
    Vue.component(name, VueGL[name]);
});

Vue.config.productionTip = false;

new Vue({
    store,
    render: h => h(App),
    mounted: function() {
        let app = this;

        window.addEventListener("keydown", function(e) {
            switch (e.code) {
                case "ArrowLeft":
                case "KeyA":
                    app.cameraPanLeft();
                    break;
                case "ArrowRight":
                case "KeyD":
                    app.cameraPanRight();
                    break;
                case "ArrowUp":
                case "KeyW":
                    app.cameraPanUp();
                    break;
                case "ArrowDown":
                case "KeyS":
                    app.cameraPanDown();
                    break;
                case "Minus":
                    app.cameraZoomDecrease();
                    break;
                case "Equal":
                    app.cameraZoomIncrease();
                    break;
            }
        });

        window.addEventListener("resize", function() {
            app.cameraCenter();
        });
    },
    methods: {
        ...mapActions([
            "cameraPanUp",
            "cameraPanDown",
            "cameraPanLeft",
            "cameraPanRight",
            "cameraZoomDecrease",
            "cameraZoomIncrease",
            "cameraCenter"
        ])
    }
}).$mount("#app");
